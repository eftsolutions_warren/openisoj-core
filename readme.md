# What is this

OpenIsoJ is a flexible framework for building an ISO 8583 message.

# Install with Maven

    <dependency>
        <groupId>org.bitbucket.openisoj</groupId>
        <artifactId>openisoj-core</artifactId>
        <version>1.1.4</version>
    </dependency>

Check [search.maven.org](http://search.maven.org/#search%7Cga%7C1%7Ca%3A%22openisoj-core%22) for the latest version.
