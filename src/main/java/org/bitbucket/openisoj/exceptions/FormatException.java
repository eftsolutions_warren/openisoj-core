package org.bitbucket.openisoj.exceptions;

public class FormatException extends Exception {
	public FormatException(String message) {
		super(message);
	}

}
