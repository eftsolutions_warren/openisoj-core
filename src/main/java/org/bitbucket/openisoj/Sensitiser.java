package org.bitbucket.openisoj;

/**
 * An interface describing how a field sensitiser works
 * 
 * @author John Oxley &lt;john.oxley@gmail.com&gt;
 */
public interface Sensitiser {
	String sensitise(String data);
}
